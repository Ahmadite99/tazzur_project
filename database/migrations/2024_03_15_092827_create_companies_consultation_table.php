<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesConsultationTable extends Migration {

	public function up()
	{
		Schema::create('companies_consultation', function(Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->softDeletes();
			$table->biginteger('advisor_id')->unsigned();
			$table->biginteger('company_id')->unsigned();
			$table->string('status');
			$table->string('message');
			$table->string('topic');
			$table->string('type');
		});
	}

	public function down()
	{
		Schema::drop('companies_consultation');
	}
}
