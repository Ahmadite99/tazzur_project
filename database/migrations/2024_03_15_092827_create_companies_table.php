<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	public function up()
	{
		Schema::create('companies', function(Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->softDeletes();
			$table->enum('status', array('waiting', 'acceptable', 'rejected', 'banned'));
			$table->string('name')->nullable();
			$table->string('phone')->nullable();
			$table->string('topic')->nullable();
            $table->json('location_map')->nullable();
			$table->string('location')->nullable();
			$table->string('fax')->nullable();
			$table->string('documents')->nullable();
			$table->json('type')->nullable();
			$table->string('logo')->nullable();
			$table->string('otp_code')->nullable();
            $table->string('email')->unique()->nullable();
            $table->boolean('is_complete')->default(false);
            $table->string('email_verification');
            $table->boolean('is_verified')->default(false);
            $table->string('password');
			$table->text('about_us')->nullable();
		});
	}
    // $table->json('location');
    // $table->string('fax');
    // $table->json('documents');
    // $table->string('type');
    // $table->string('logo');
    // $table->string('otp_code');
    // $table->string('email');
    // $table->string('email_verification');
    // $table->string('about_us');

	public function down()
	{
		Schema::drop('companies');
	}
}
