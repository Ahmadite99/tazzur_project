<?php

namespace Database\Seeders;
use Faker\Factory as Faker;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\Manager::create([
            'name' => 'Test User',
            'email' => 'manager@example.com',
            'password' => bcrypt('12345678'),
            'role_name' => 'admin',
        ]);
        $this->call([
            UsersTableSeeder::class,
            CompaniesTableSeeder::class,
            JobsTableSeeder::class,
            CoursesTableSeeder::class,
            CourseApplicationsTableSeeder::class,
            JobApplicationsTableSeeder::class,
            AdvisorTableSeeder::class,

            FormOptionTableSeeder::class,
            FormQuestionTableSeeder::class,
            FormTableSeeder::class,
        ]);
    }
}
