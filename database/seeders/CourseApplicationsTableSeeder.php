<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class CourseApplicationsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        for ($i = 1; $i <= 10; $i++) {
            DB::table('course_applications')->insert([
                'user_id' => $i,
                'course_id' => $i,
                'status' => 'applied',
            ]);
        }
    }
}

