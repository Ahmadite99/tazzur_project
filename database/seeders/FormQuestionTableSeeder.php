<?php

namespace Database\Seeders;

use App\Models\Form;
use App\Models\FormQuestion;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Faker\Factory as Faker;
class FormQuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $forms = Form::all();
        $faker = Faker::create();
        foreach ($forms as $form) {
            FormQuestion::create([
                'form_id' => $form->id,
                'question' => "Sample Question $form->id",
            ]);
        }
    }
}
