<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class JobsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        for ($i = 1; $i <= 10; $i++) {
            DB::table('job')->insert([
                'hidden_name' => $faker->boolean,
                'job_title' => 'Software Developer',
                'number_employees' => rand(1, 50),
                'topic' => $faker->randomElement(['Information Technology', 'Finance', 'Marketing']),
                'job_environment' => $faker->randomElement(['online', 'offline']),
                'salary_fields' => '1000',
                'education_level' => 'Bachelor\'s Degree',
                'require_qualifications' => 'Strong programming skills',
                'special_qualifications' => 'Experience with Laravel framework',
                'is_required_image' => $faker->boolean,
                'required_languages' => $faker->randomElement(['English', 'Spanish', 'French']),
                'experiense_years' => $faker->randomElement(['10 Years', '1 Year', '2 Years']),
                'gender' => $faker->randomElement(['Male', 'Female']),
                'location' => $faker->city,
                'company_id' => rand(1, 10),
                'is_required_license' => $faker->boolean,
                'status' => $faker->randomElement(['current', 'finite','حالية','منتيهة']),
                'is_required_military' => $faker->randomElement(['finished', 'in service']),
                'job_time' =>$faker->randomElement(['full_time', 'part_time']),
                'end_date' => '2024-12-31',
                //views
            ]);
        }
    }
}

