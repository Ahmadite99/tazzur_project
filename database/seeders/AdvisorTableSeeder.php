<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class AdvisorTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        for ($i = 1; $i <= 10; $i++) {
            DB::table('advisors')->insert([

                'name' => "Company Name {$i}",
                'email' => "company{$i}@example.com",
                'password' => bcrypt('password'),
                'topics' => "Software",
            ]);
        }
    }
}
