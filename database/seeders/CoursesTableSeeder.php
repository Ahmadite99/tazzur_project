<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class CoursesTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        for ($i = 1; $i <= 10; $i++) {
            DB::table('courses')->insert([
                'duration' => "{$i} hours",
                'number_trainees' => "{$i}0",
                'topic' => "Topic{$i}",
                'type' => "Type{$i}",
                'name' => "Course Name {$i}",
                'company_id' => $i,
                'start_date' => now(),
                'end_date' => now()->addDays($i),
                'days' => $i,
                'price' => $i * 100.0,
             //   'views' => $i * 10,
                'location' => "Location{$i}",
                'status'=>$faker->randomElement(['current', 'finite','حالية','منتيهة']),
            ]);
        }
    }
}
