<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class CompaniesTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        for ($i = 1; $i <= 10; $i++) {
            DB::table('companies')->insert([
                'status' => 'acceptable',
                'name' => "Company Name {$i}",
                'phone' => "123456789{$i}",
                'topic' => "Topic{$i}",
                'location_map' => json_encode(['lat' => 123.123, 'lng' => 456.456]),
                'location' => "Location{$i}",
                //? location
                'fax' => "Fax{$i}",
                'documents' => "Documents{$i}",
                'type' => json_encode(["Type{$i}"]),
                'logo' => "Logo{$i}",
                'otp_code' => str_pad($i, 4, '0', STR_PAD_LEFT),
                'email' => "company{$i}@example.com",
                'is_complete' => true,
                'email_verification' => $faker->sha1,
                // 'is_verified' => $i % 2 == 0,
                'password' => bcrypt('password'),
                'about_us' => "About Company {$i}",
            ]);
        }
    }
}
