<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class JobApplicationsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        for ($i = 1; $i <= 10; $i++) {
            DB::table('job_applications')->insert([
                'user_id' => $i,
                'job_id' => $i,
                'status' => 'pending',
                'priority_application' => $i % 2 == 0 ? 1 : 2,
            ]);
        }
    }
}
