<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;


use Laravel\Sanctum\HasApiTokens;

class Company extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $guard='company';
    protected $table='companies';
    protected $fillable = [
        'status',
        'name',
        'phone',
        'topic',
        'location',
        'fax',
        'documents',
        'type',
        'logo',
        'otp_code',
        'email',
        'email_verification',
        'about_us',
    ];

    protected $casts = [
        'location' => 'json',
        'documents' => 'json',
    ];
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    public function courses()
{
    return $this->hasMany(Course::class);
}

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
