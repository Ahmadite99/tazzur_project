<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use HasFactory;

    protected $table = 'job';

    protected $fillable = [
        'hidden_name',
        'job_title',
        'number_employees',
        'topic',
        'job_environment',
        'salary_fields',
        'education_level',
        'require_qualifications',
        'special_qualifications',
        'is_required_image',
        'required_languages',
        'experiense_years',
        'gender',
        'location',
        'company_id',
        'is_required_license',
        'status',
        'is_required_military',
        'job_time',
        'views',
        'end_date',
    ];

    // Define relationships if applicable
    public function forms()
    {
        return $this->hasMany(Form::class, 'job_id');
    }
    public function jobapplications()
    {
        return $this->hasMany(JobApplication::class, 'job_id');
    }
    public function company()
{
    return $this->belongsTo(Company::class);
}

}
