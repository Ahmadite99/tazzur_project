<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $table = 'courses';

    protected $fillable = [
        'duration',
        'number_trainees',
        'topic',
        'type',
        'name',
        'company_id',
        'start_date',
        'end_date',
        'days',
        'price',
        'status',
        'location',
    ];
    public function courseApplications()
{
        return $this->hasMany(CourseApplication::class);
}

    public function company()
{
    return $this->belongsTo(Company::class);
}

}
