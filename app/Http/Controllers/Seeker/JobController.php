<?php

namespace App\Http\Controllers\Seeker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\Company;
use App\Models\Form;
use App\Models\FormQuestion;
use App\Models\FormOption;
use App\Models\UserQuestion;
use App\Models\JobApplication;
use App\Models\User;
use DB;
use Illuminate\Pagination\Paginator;
class JobController extends Controller
{


    public function my_jobs()
{
    $page = request()->query('page', 1);
    $user_id = auth()->id();
    $user = User::with('user_cv')->find($user_id);
    $user_cv = $user->user_cv;
    if(!$user_cv){
        return response()->json(['message'=>'please complete your information'], 400);
    }
    $topics = json_decode($user->topic, true);
    $workcity = json_decode($user_cv->work_city, true);
    $jobQuery = Job::query();

    if (!empty($topics)) {
        $jobQuery->whereIn('topic', $topics);
    }

    if (!empty($user->experience_years)) {
        $jobQuery->orWhere('experiense_years', $user->experience_years);
    }

    if (!empty($user->gender)) {
        $jobQuery->orWhere('gender', $user->gender);
    }

    if (!empty($user->driving_license)) {
        $jobQuery->orWhere('is_required_license', $user->driving_license);
    }

    if (!empty($workcity)) {
        $jobQuery->orWhereIn('location', $workcity);
    }

    $jobs = $jobQuery->distinct()
        ->with('company:id,name,logo,location,about_us');



    $currentJobs = $jobs->whereIn('status', ['current', 'حالية'])->paginate(10);;

    return $currentJobs;
}

    public function job_details($id)
{
        $job = Job::where('id', $id)->with('company:id,name,logo,location,about_us')->firstOrFail();
        return $job;
}
    public function all_jobs()
{
    $page = request()->query('page', 1);
    $jobs = Job::whereIn('status', ['current', 'حالية'])
        ->distinct()
        ->with('company:id,name,logo,location,about_us')
        ->paginate(10);

    return $jobs;
}
    public function filter(Request $request)
{
    $validator = $request->validate([
        'topic',
        'experience_years',
        'work_city',
    ]);

    $topicArray = $request->topic ?? [];
    $experienceYearsArray = $request->experience_years ?? [];
    $workCityArray = $request->work_city ?? [];

    $page = request()->query('page', 1);
    $jobQuery = Job::whereIn('topic', $topicArray)
        ->orWhereIn('experiense_years', $experienceYearsArray)
        ->orWhereIn('location', $workCityArray)
        ->distinct()
        ->with('company:id,name,logo,location,about_us')
        ->paginate(10);

    $currentJobs = $jobQuery->whereIn('status', ['current', 'حالية']);

    return $currentJobs;
}
    public function search(Request $request)
{
    $validator = $request->validate([
        'value' => 'required',
    ]);

    $page = request()->query('page', 1);
    $var = '%' . $request->value . '%';
    $company = Company::where('name', 'like', $var)->pluck('id');
    $value = Job::where('topic', 'like', $var)
        ->orWhere('location', 'like', $var)
        ->orWhereIn('company_id', $company)
        ->distinct()
        ->with('company:id,name,logo,location,about_us')
        ->paginate(10);

    $currentJobs = $value->whereIn('status', ['current', 'حالية']);

    return response()->json($currentJobs, 200);
}
    public function show_questions($id)
{
    $forms = Form::where('job_id', $id)->get();
    $formIds = $forms->pluck('id')->toArray();
    $questions = FormQuestion::whereIn('form_id', $formIds)->with('options')->get();

    return $questions;
}

    public function answer_and_applay(Request $request,$id)
{
    $validator = $request->validate([
        'questions'
    ]);
    $user_id=auth()->id();

    $isexist=JobApplication::where([
        ['user_id',$user_id],
        ['job_id',$id]
    ])->first();
    if ($isexist){
        return response()->json(['message'=>'Sorry, you have already applied for the job'], 400);
    }

    $form=Form::where('job_id',$id)->first();
    foreach($request->questions as $question){
        $user_question=new UserQuestion();
        $user_question->user_id=$user_id;
        $user_question->question_id= $question['id'];
        $user_question->form_options_id=$question['answer_id'];
        $user_question->form_id=$form->id;
        $user_question->save();
    }
    //job application
        $job_application = new JobApplication();
        $job_application->user_id=$user_id;
        $job_application->job_id=$id;
        $job_application->save();

        return response()->json(['message'=>'You have successfully applied for the job'], 200);
    }

}
