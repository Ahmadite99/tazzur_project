<?php

namespace App\Http\Controllers\Seeker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Company;
use App\Models\Course;
use App\Models\CourseApplication;
use App\Models\User;

class CourseController extends Controller
{
    public function courses()
    {
        $page = request()->query('page', 1);
        $courses = Course::whereIn('status', ['current', 'حالية'])
            ->distinct()
            ->with('company:id,name,logo,location,about_us')
            ->paginate(10);

        return $courses;

    }

    public function course_details($id)
    {
        $job = Course::where('id', $id)->with('company:id,name,logo,location,about_us')->firstOrFail();
        return $job;
    }
    public function interested($id)
    {
        $user_id = auth()->id();
        $isexist=CourseApplication::where([
            ['user_id',$user_id],
            ['course_id',$id]
        ])->first();
        if ($isexist){
            return response()->json(['message'=>'Sorry,  You was interested in this course before'], 400);
        }

        $interested=new CourseApplication();
        $interested->user_id=$user_id;
        $interested->course_id=$id;
        $interested->save();
        return response()->json(['message'=>'You have successfully interested for the course'], 200);
    }

}
