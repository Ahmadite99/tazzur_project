<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Survey;
use App\Models\SurveyOption;
use App\Http\Controllers\BaseController;
class SurveyController extends BaseController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $company_id = auth()->guard('company')->user()->id;
        $surveys = Survey::where('company_id', $company_id)->with('options')->get();
        return $this->sendResponse($surveys, 'surveys');
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'options' => 'required|array',
        //    'options.*' => 'required|string',
        ]);

        $survey = new Survey;
        $survey->company_id = auth()->guard('company')->user()->id;
        $survey->title = $request->title;
        $survey->save();

        foreach ($request->options as $option_text) {
            $option = new SurveyOption;
            $option->survey_id = $survey->id;
            $option->option_text = $option_text;
            $option->vote_count = 0; // بدء العد من الصفر
            $option->save();
        }
        return $this->sendResponse($survey, 'Survey created successfully!',200);
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $survey = Survey::where('id', $id)->with('options')->first();

        if (!$survey) {
            return response()->json(['message' => 'Survey not found'], 404);
        }
        return $this->sendResponse($survey, 'Survey Details!',200);

    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title' => 'sometimes|required|string',
            'options' => 'sometimes|required|array',
           // 'options.*.id' => 'sometimes|required|exists:surveys_options,id',
           // 'options.*.option_text' => 'sometimes|required|string',
        ]);

        $survey = Survey::find($id);
        if (!$survey) {
            return response()->json(['message' => 'Survey not found'], 404);
        }
        if ($request->has('title')) {
            $survey->title = $request->title;
        }
        $survey->save();
        if ($request->has('options')) {
            foreach ($request->options as $option) {
                $surveyOption = SurveyOption::find($option['id']);
                if ($surveyOption) {
                    $surveyOption->option_text = $option['option_text'];
                    $surveyOption->save();
                }
            }
        }
        return $this->sendResponse($survey, 'Survey updated successfully!',200);
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $survey = Survey::find($id);

        if (!$survey) {
            return response()->json(['message' => 'Survey not found'], 404);
        }

        // حذف الخيارات أولًا
        $survey->options()->delete();
        $survey->delete();

        return response()->json(['message' => 'Survey deleted successfully']);
    }

}
