<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
class ApplicationJobController extends BaseController
{
    public function applications($job_id)
    {
        $applications = \App\Models\JobApplication::with('user')
            ->where('job_id', $job_id)
            ->get(['user_id', 'status', 'priority_application']) // اختياري لتقليل حجم البيانات
            ->map(function ($application) {
                return [
                    'name' => $application->user->first_name . ' ' . $application->user->last_name,
                    // 'photo' => $application->user->photo, // إذا كان هناك عمود للصور في جدول المستخدمين
                ];
            });

            return $this->sendResponse($applications, 'applications');
    }
    public function details_application($application_id)
    {
        $application = \App\Models\JobApplication::with('user')
            ->where('id', $application_id)
            ->first();

        if (!$application) {
            return $this->sendError(404,'Application not found.');
        }

        return $this->sendResponse($application->user, 'application');
    }

    public function accept_refuse(Request $request, $application_id)
    {
        $request->validate([
            'status' => 'required|in:accepted,rejected', // تأكد من مطابقة هذه القيم لما هو مُعرّف في قاعدة البيانات
        ]);

        $application = \App\Models\JobApplication::find($application_id);

        if (!$application) {
            return $this->sendError(404,'Application not found.');
        }

        $application->status = $request->status;
        $application->save();
        return $this->sendResponse($request->status, "Application has been {$request->status}.");

    }



}
