<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CompanyConsulution;
use App\Models\Advisor;

use Auth;
use App\Http\Controllers\BaseController;
class ConsulutionController extends BaseController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::guard('company')->user();
        $advisors = Advisor::whereHas('consultations', function($query) use ($user) {
            $query->where('company_id', $user->id);
        })->get();
        $data = ['advisors' => $advisors,];
        return $this->sendResponse($advisors, 'advisors');
    }
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = Auth::guard('company')->user();
        $consultations = CompanyConsulution::where('company_id', $user->id)
                                      ->where('advisor_id', $id)
                                      ->orderBy('created_at')
                                      ->get();
        return $this->sendResponse($consultations, 'consultations');
    }
    public function store(Request $request)
    {
        // $validator=$request->validate([
        //     'message'=>'required',
        //     'topic'=>'required'
        // ]);
        // if ($validator->fails()) {
        //     return response()->json(['errors' => $validator->errors()], 422);
        // }
        $company = Auth::guard('company')->user();

        $consulution = new CompanyConsulution();
        $consulution->status='waiting';
        $consulution->message=$request->message;
        $consulution->company_id=$company->id;
        $consulution->advisor_id='1';//test
        $consulution->topic=$request->topic;
        $consulution->type='company';
        $consulution->save();
        $data = ['consulution' => $consulution,];
        return $this->sendResponse($data, 'The registration process was completed successfully');
    }
  public function update(Request $request, string $id)
    {
        //
    }
    public function destroy(string $id)
    {
        //
    }
}
