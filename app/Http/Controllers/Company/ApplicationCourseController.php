<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
class ApplicationCourseController extends BaseController
{
    public function applications($course_id)
    {
        $applications = \App\Models\CourseApplication::with('user')
            ->where('course_id', $course_id)
            ->get()
            ->map(function ($application) {
                return [
                    'name' => $application->user->first_name . ' ' . $application->user->last_name,
                    'image' => $application->user->user_cv->image ?? 'fd.jpg',
                ];
            });
            return $this->sendResponse($applications, 'applications');
    }

    public function details_application($application_id)
    {
        $application = \App\Models\CourseApplication::with('user')
            ->where('id', $application_id)
            ->first();

        if (!$application) {
            return $this->sendError(404,'Application not found.');
        }
        return $this->sendResponse($application->user, 'application');
    }

    public function accept_refuse(Request $request, $application_id)
    {
        $request->validate([
            'status' => 'required|in:accepted,rejected', // تأكد من أن الحالات المسموح بها مطابقة لقاعدة البيانات
        ]);
        $application = \App\Models\CourseApplication::find($application_id);
        if (!$application) {
            return $this->sendError(404,'Application not found.');
        }
        $application->status = $request->status;
        $application->save();
        return $this->sendResponse($request->status, "Application has been {$request->status}.");
    }
}
