<?php

namespace App\Http\Controllers\Company;


use App\Models\Company;
use App\Models\Verification;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Hash;
use Auth;

class AuthController extends BaseController
{
    public function register(Request $request)
    {
        $validator=$request->validate([
            'email' => 'required|email|unique:users,email',
            'password' => ['required', 'confirmed', 'min:8','regex:/^(?=.*[a-zA-Z])(?=.*\d).+$/']
        ]);
        // if ($validator->fails()) {
        //     return response()->json(['errors' => $validator->errors()], 422);
        // }
 //       Your submission contains invalid data. Please review and correct it.
        $company = Company::where('email', $validator['email'])->first();
        if ($company && !$company->is_verified)
        {
            $verification = new Verification();
            $verification->code=rand(1000, 9999);
            $verification->email=$request->email;
            $verification->save();
            return $this->sendError(400,'Verification code required. Please verify the code sent to your email.');
        }
        elseif ($company && $company->is_verified)
        {
            if(! $company->is_complete)
            {
                return $this->sendError(400,'Registration incomplete. Please ensure all required fields are filled.');
            }
            return $this->sendError(409,'A company with these details already exists..');
        }
        else
         {
        $company = new Company();
        $company->email =  $validator['email'];
        $company->password = Hash::make($validator['password']);
        $company->save();

        $verification = new Verification();
        $verification->code=rand(1000, 9999);
        $verification->email=$request->email;
        $verification->save();
       // Send verification email
       //Mail::to($company->email)->send(new RegistercompanyMail($company,$company->verification_code));
       $credentials = request(['email', 'password']);
       $token = auth()->guard('company')->attempt($credentials);
       $data = [
                'code' => $verification->code,
    ];
    return $this->sendResponse($data, 'The registration process was completed successfully');
    }
    }
    public function verify_code(Request $request)
    {
        $validator=$request->validate([
            'code'=>'required',
            'email'=>'required'
        ]);
        $company=company::where('email',$request->email)->first();
        $customClaims = ['guard' => 'company'];
        $token = auth('company')->claims($customClaims)->login($company);
        $verification = Verification::where([
            ['code', $request->code],
            ['email',$request->email]
           ])->first();
           if ($verification && $company->email_verified_at==null){
            $company->update(['is_verified' => true]);
            $company->save();
           }
        //    if ($verification && $verification->expire_at=true){
        //    //expired
        //    }
           if ($verification) {
               $company->update(['is_verified' => true]);
               $verification->delete();
               return $this->sendResponse($token, 'The account has been activated successfully');
           } else {
            return $this->sendError(400,'The verification code is incorrect. Please try again.');
           }
    }
    public function resend_code(Request $request)
    {
        $validator = $request->validate([
            'email' => 'required|email',
        ]);

        $verification = Verification::where('email',$request->email)->first();

        $today = Carbon::today();
        $lastVerificationDate = $verification->last_resend_date ? Carbon::parse($verification->last_resend_date) : null;
        if (!$lastVerificationDate || !$lastVerificationDate->isSameDay($today)) {
            $verification->num_of_resend = 0;
        }
        if ($verification->num_of_resend >= 2) {
            return $this->sendError(400,'You cannot re-send the verification code more than twice per day.');

        } else {
            $verification->code = rand(1000, 9999);
            $verification->num_of_resend += 1;
            $verification->last_resend_date = $today;
            $verification->save();

            //Mail::to($verification->email)->send(new RegistercompanyMail($verification, $verification->code));
            $data = [
                'code' => $verification->code,
            ];
        }
        return $this->sendResponse($data, 'The code has been sent successfully');

    }
    public function complete_register(Request $request)
    {
        $rules = [
            'name' => 'required|string',
          //  'phone' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
          'phone' => 'nullable|integer',
            'topic' => 'required|string',
            'company_type' => 'required|array',
            'documents' => 'required|pdf',
            'about' => 'required|string',
            'image' => 'required|image|max:2048|dimensions:min_width=100,min_height=100',
            'location' => 'required|string',
            'location_map' => 'nullable|array',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $docPath = '43';
        $imagePath ='4343';// $request->file('image')->store('images');
        $company = auth()->guard('company')->user();
        $company->name = $request->name;
        $company->phone = $request->phone;
        $company->topic = $request->topic;
        $company->type = json_encode($request->company_type);
        $company->documents = $docPath;
        $company->logo = $imagePath;
        $company->about_us = $request->about;
        $company->location = $request->location;
        if ($request->has('location_map')) {
            $company->location_map = json_encode($request->location_map);
        }
        $company->save();
        $company->update(['is_complete' => true]);
        return $this->sendResponse($company, 'Company registered successfully');
    }

    public function login(Request $request)
    {
        $validator=$request->validate([
            'email'=>'required|email',
            'password'=>'required'
        ]);
        $credentials = request(['email', 'password']);
        $company = company::where('email', $validator['email'])->first();
        if ($company && $company->is_verified && !$company->is_complete) {
            return $this->sendError(400,'Please complete all required fields to proceed with login.');
        }if ($company && ! $company->is_verified) {
            return $this->sendError(400,'Email verification required. Please verify to proceed.');
        }
        if (!$company || !Hash::check($credentials['password'], $company->password)) {
            return $this->sendError(400,'The data provided is invalid. Please review your input.');
        }
        if($company && $company->status == 'blocked'){
            return $this->sendError(403,'Your account has been blocked. Please contact support for further assistance.');
         }
         if($company && $company->status == 'waiting'){
            return $this->sendError(202,'Your request is being processed. Please wait for further instructions.');
         }

        if (!$company) $this->sendError(404,'The account you are trying to access does not exist.');
        else if ($company)
        {
            if(Hash::check($validator['password'], $company->password)) {

            $token = auth()->guard('company')->attempt($credentials);
            $data = [
                'company' => $company,
                'token' => $token
             ];
             return $this->sendResponse($data, 'You have been logged in successfully');

        }
        else
         return $this->sendError(401,'Authentication failed, please check your password.');
        }
    }

    public function forget_password(Request $request)
    {
        $validator=$request->validate([
            'email'=>'required|email',
        ]);
        $company=Company::where('email',$request->email)->first();
        if($company)
        {
            $Password=Verification::updateOrCreate(
                ['email'=>$request->email ],
                    [
                        'email'=>$request->email,
                        'code'=>rand(1000, 9999),
                    ]
                    );
                    $code=['code'=>$Password->code];
         // Mail::to($company->email)->send(new ForgottenPassword($Password));
         return $this->sendResponse($code, 'confirmation code has been sent successfull');
        }
        else
        {
            return $this->sendError(404,'The email address entered does not match any account. Please check and try again.');

        }

    }

    public function reset_password(Request $request)
    {
        $validator=$request->validate([
            'email'=>'required|email',
            'password'=>'required|confirmed',
        ]);
        $company=company::where('email',$request->email)->first();
         if(!$company)
         {
            return $this->sendError(404,'No company associated with the provided information was found.');
         }
         else
         {
         $company->password=bcrypt($request->password);
         $company->save();
         return $this->sendResponse('done', 'Reset Password Successfully!');
         }
         return $this->sendError(404,'The email address entered does not match any account. Please check and try again.');
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);

    }
    public function delete_account(Request $request)
    {
       //token - password
    }

    public function get_information_cv(){}
    public function profile(){}
    public function update_profile(){}
    public function change_password(){}




}
