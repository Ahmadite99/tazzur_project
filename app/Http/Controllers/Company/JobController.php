<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Job;
use App\Http\Controllers\BaseController;
class JobController extends BaseController
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $status = $request->input('status', 'current');
     //   $query = Job::where('company_id', auth()->guard('company')->user()->id);
      $query = Job::get();
        // if ($status === 'current') {
        //     $query->where('status', '=', 'current');
        // } elseif ($status === 'expired') {
        //     $query->where('status', '=', 'expired');
        // }

        $jobs = $query;
        return $this->sendResponse($jobs, 'jobs');
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'job_title' => 'required|string',
            'number_employees' => 'required|string',
            // تحقق من باقي الحقول بنفس الطريقة
            'end_date' => 'required|date',
        ]);

        $job = new Job();
        $job->fill($request->all());
        $job->company_id = auth()->guard('company')->user()->id;
        $job->save();
        return $this->sendResponse($job, 'Job created successfully!');
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $job = Job::where('id', $id)->where('company_id', auth()->guard('company')->user()->id)->first();

        if (!$job) {
            return $this->sendError(404,'Job not found');

        }
        return $this->sendResponse($job, 'Job Details!');
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            // قواعد التحقق من الصحة للحقول المطلوب تحديثها
        ]);

        $job = Job::where('id', $id)->where('company_id', auth()->guard('company')->user()->id)->first();

        if (!$job) {
            return $this->sendError(404,'Job not found');
        }

        $job->update($request->all());
        return $this->sendResponse($job, 'Job updated successfully!');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $job = Job::where('id', $id)->where('company_id', auth()->guard('company')->user()->id)->first();

        if (!$job) {
            return $this->sendError(404,'Job not found');
        }

        $job->delete();
        return $this->sendResponse(true, 'Job deleted successfully!');

    }

}
