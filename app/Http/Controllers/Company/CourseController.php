<?php
namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Course; // Make sure to use your actual Course model path
use App\Http\Controllers\BaseController;
class CourseController extends BaseController
{
    /**
     * Display a listing of the courses.
     */
    public function index()
    {
        $courses = Course::all();
        return $this->sendResponse($courses, 'courses');
    }

    /**
     * Store a newly created course in storage.
     */
    public function store(Request $request)
    {
        $course = new Course();
        $course->duration = $request->duration;
        $course->number_trainees = $request->number_trainees;
        $course->topic = $request->topic;
        $course->type = $request->type;
        $course->name = $request->name;
        $course->company_id = $request->company_id;
        $course->start_date = $request->start_date;
        $course->end_date = $request->end_date;
        $course->days = $request->days;
        $course->price = $request->price;
        $course->views = $request->views;
        $course->location = $request->location;

        $course->save();
        return $this->sendResponse($course, 'course');
    }

    /**
     * Display the specified course.
     */
    public function show(string $id)
    {
        $course = Course::find($id);
        if (!$course) {
            return $this->sendError(404,'Course not found.');
        }
        return $this->sendResponse($course, 'course');
    }

    /**
     * Update the specified course in storage.
     */
    public function update(Request $request, string $id)
    {
        $course = Course::find($id);
        if (!$course) {
            return $this->sendError(404,'Course not found.');

        }

        $course->fill($request->all()); // This assumes $fillable is properly defined in your Course model
        $course->save();
        return $this->sendResponse($course, 'course');
    }

    /**
     * Remove the specified course from storage.
     */
    public function destroy(string $id)
    {
        $course = Course::find($id);
        if (!$course) {
            return $this->sendError(404,'Course not found.');
        }

        $course->delete();
        return $this->sendResponse(true, 'Course deleted successfully');

    }
}
