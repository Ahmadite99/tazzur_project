<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Manager; // استيراد نموذج Manager
use App\Http\Controllers\BaseController;
class CoordinatorController extends BaseController
{
    /**
     * عرض قائمة بجميع المديرين.
     */
    public function index()
    {
        $managers = Manager::all();
        return $this->sendResponse($managers, 'managers');
    }

    /**
     * إضافة مدير جديد إلى قاعدة البيانات.
     */
    public function store(Request $request)
    {
        // يمكنك هنا إضافة التحقق من صحة البيانات
        $manager = Manager::create($request->all());
        return $this->sendResponse($manager, 'manager');
    }

    /**
     * عرض بيانات مدير محدد.
     */
    public function show(string $id)
    {
        $manager = Manager::findOrFail($id);
        return $this->sendResponse($manager, 'manager');

    }

    /**
     * تحديث بيانات مدير محدد في قاعدة البيانات.
     */
    public function update(Request $request, string $id)
    {
        $manager = Manager::findOrFail($id);
        $manager->update($request->all());
        return $this->sendResponse($manager, 'manager');
    }

    /**
     * حذف مدير من قاعدة البيانات.
     */
    public function destroy(string $id)
    {
        Manager::findOrFail($id)->delete();
        return $this->sendResponse(null, 'done');
    }
}
