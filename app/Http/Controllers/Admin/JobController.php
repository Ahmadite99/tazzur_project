<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Job;

class JobController extends Controller
{
    public function index()
    {
        $jobs = Job::all();
        return response()->json($jobs);
    }

    public function show(string $id)
    {
        $job = Job::findOrFail($id);
        return response()->json($job);
    }

    public function accept_refuse(string $id)
    {
        // يمكن تعريف المنطق هنا لقبول أو رفض وظيفة بناءً على معايير محددة
        // هذا سيتطلب تفاصيل إضافية حول كيفية تنفيذ هذه الوظيفة
    }
}
