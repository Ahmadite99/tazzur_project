<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\BaseController;
class CompanyController extends BaseController
{
    /**
     * Display a listing of the companies.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->query('status', 'waiting'); // Default to 'waiting' if no status is provided
        //$companies = Company::where('status', $status)->get();
        $companies = Company::get();
        return $this->sendResponse($companies, 'companies');
    }

    /**
     * Display the specified company.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $company = Company::findOrFail($id);
        if($company)
        {
            return $this->sendResponse($company, 'company');
        }
        else
        {
            return $this->sendError(404,'company not found.');
        }

    }

    /**
     * Update the specified company in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $id)
    {
        $company = Company::findOrFail($id);
        $company->update($request->all());
        return $this->sendResponse($company, 'Company updated successfully.');
    }

    /**
     * Remove the specified company from storage.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        $company = Company::findOrFail($id);
        $company->delete();
        return response()->json(['message' => 'Company deleted successfully.']);
    }

    /**
     * Accept or reject a company.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function set_status(Request $request, string $id)
    {
        $company = Company::findOrFail($id);
        $status = $request->input('status'); // Expect 'acceptable' or 'rejected'
        $company->status = $status;
        $company->save();
        return $this->sendResponse($company, "Company status updated to $status.");
    }
    /**
     * Block or unblock a company.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */

}
