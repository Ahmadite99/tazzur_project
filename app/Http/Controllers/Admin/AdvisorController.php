<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Advisor;
use App\Http\Controllers\BaseController;
class AdvisorController extends BaseController
{
    /**
     * عرض قائمة بجميع المستشارين.
     */
    public function index()
    {
        $advisors = Advisor::all();
        return $this->sendResponse($advisors, 'advisors');
    }

    /**
     * إضافة مستشار جديد إلى قاعدة البيانات.
     */
    public function store(Request $request)
    {
        $advisor = Advisor::create($request->all());
        return $this->sendResponse($advisor, 'advisor');
    }

    /**
     * عرض بيانات مستشار محدد.
     */
    public function show(string $id)
    {
        $advisor = Advisor::findOrFail($id);
        return $this->sendResponse($advisor, 'advisor');

    }

    /**
     * تحديث بيانات مستشار محدد في قاعدة البيانات.
     */
    public function update(Request $request, string $id)
    {
        $advisor = Advisor::findOrFail($id);
        $advisor->update($request->all());
        return $this->sendResponse($advisor, 'advisor');
    }

    /**
     * حذف مستشار من قاعدة البيانات.
     */
    public function destroy(string $id)
    {
        Advisor::findOrFail($id)->delete();
        return $this->sendResponse(null, 'done');
    }
}
