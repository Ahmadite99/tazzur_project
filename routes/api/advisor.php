<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Advisor\ConsulutionCompanyController;
use App\Http\Controllers\Advisor\ConsulutionSeekerController;
use App\Http\Controllers\Advisor\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::controller(AuthController::class)->group(function () {
    Route::post('add_addvisor', 'add_addvisor');
    Route::post('login', 'login');
    Route::post('logout', 'logout')->middleware(['auth:advisors']);
});
Route::middleware(['api','auth:advisors','scopes:advisor'])->group(function ()
{
    Route::apiResources([
        'consulution_users' => ConsulutionCompanyController::class,
        'consulution_company' =>ConsulutionSeekerController::class,
    ]);
    Route::controller(ConsulutionCompanyController::class)->group(function () {
        Route::post('answer/{id}', 'answer');
    });
    Route::controller(ConsulutionSeekerController::class)->group(function () {
        Route::post('answer/{id}', 'answer');
    });
});
