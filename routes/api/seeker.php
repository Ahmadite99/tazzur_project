<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Seeker\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::controller(AuthController::class)->group(function () {
    Route::post('register', 'register');
    Route::post('login', 'login');
    Route::post('forget_password', 'forget_password');  
    Route::post('confirm_reset_password', 'confirm_reset_password');
    Route::post('resend_code', 'resend_code');  
    Route::post('verify_code', 'verify_code');
    Route::group(['middleware'=>['auth:api']],function (){   
    Route::post('complete_register', 'complete_register');
    Route::post('logout', 'logout');
});
});


