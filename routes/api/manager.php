<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\AdvisorController;
use App\Http\Controllers\Admin\CoordinatorController;
use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\JobController;
use App\Http\Controllers\Admin\SeekerController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\ApplicationCourseController;
use App\Http\Controllers\Admin\ApplicationJobController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('logout', 'logout')->middleware(['auth:sanctum']);

});

Route::middleware(['api','auth:manager'])->group(function () {
    Route::apiResources([
        'companies' => CompanyController::class,
        'advisors' => AdvisorController::class,
        'coordinators' => CoordinatorController::class,
        'courses' => CourseController::class,
        'jobs' => JobController::class,
        'seekers' => SeekerController::class,
    ]);
    Route::controller(CompanyController::class)->group(function () {
        Route::post('companies/set_status/{id}', 'set_status');
    });
    Route::controller(AdvisorController::class)->group(function () {
    });
    Route::controller(CoordinatorController::class)->group(function () {
    });
    Route::controller(CourseController::class)->group(function () {
        //applications (id course)
    });
    Route::controller(JobController::class)->group(function () {
        Route::post('accept_refuse/{id}', 'accept_refuse');
    });
    Route::controller(ApplicationCourseController::class)->group(function () {
        //index(applications : id course)
        Route::get('course_applications', 'index');
    });
    Route::controller(ApplicationJobController::class)->group(function () {
        Route::get('job_applications', 'index');
        Route::post('job_filter', 'filter');
        Route::post('job_transfer', 'transfer');

         //index(applications : id job)   - filter - transfer
    });
    Route::controller(SeekerController::class)->group(function () {
    Route::post('block_unblock/{id}', 'block_unblock');
    });
});
