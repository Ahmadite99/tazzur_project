 <?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Company\ApplicationCourseController;
use App\Http\Controllers\Company\ApplicationJobController;
use App\Http\Controllers\Company\AuthController;
use App\Http\Controllers\Company\ConsulutionController;

use App\Http\Controllers\Company\CourseController;
use App\Http\Controllers\Company\JobController;
use App\Http\Controllers\Company\SurveyController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('forget_password', 'forget_password');
    Route::post('confirm_reset_password', 'reset_password');
    Route::post('resend_code', 'resend_code');
    Route::post('verify_code', 'verify_code');
    Route::group(['middleware'=>['auth:company']],function (){
        Route::post('complete_register', 'complete_register')->middleware(['is_verified']);
        Route::post('logout', 'logout');
    });
});
 Route::middleware(['auth:company', 'is_verified' ,'is_complete'])->group(function ()
 {
    Route::apiResources([
        'consulutions' =>ConsulutionController::class,
        'surveys' =>SurveyController::class,
        'courses' =>CourseController::class,
        'jobs' =>JobController::class,
    ]);

  
Route::controller(ApplicationCourseController::class)->group(function () {
        Route::get('applications/course/{id}', 'applications');
        Route::get('details_application/course/{id}', 'details_application');
        Route::post('accept_refuse/course/{id}', 'accept_refuse');
    });
Route::controller(ApplicationJobController::class)->group(function () {
        Route::get('applications/{id}', 'applications');
        Route::get('details_application/{id}', 'details_application');
        Route::post('accept_refuse/{id}', 'accept_refuse');
    });
});
